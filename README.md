*README IS COMING*

Il suffit d'ouvrir le fichier "Main.py" afin de lancer le programme. 
Ce fichier fera automatiquement appel à "main_classique.py" ou "main_interaction.py" en fonction du choix d'avoir un programme interactif ou non.
"Data.py" est le fichier qui contient la classe Data. Cette classe est la source de toutes les méthodes utilisées pour analyser le contenu du fichier "iris.csv". 
"LogGenerator.py" est la classe qui permet l'écriture du fichier "log.txt".
Ce fichier log sera enregistré sur le bureau.
Le fichier "plot.pf" contient un histogramme généré par le programme. Il sera enregistré à côté du script principal.

Le fichier iris.csv est téléchargeable ici https://gist.github.com/netj/8836201 